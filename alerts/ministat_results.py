from dataclasses import dataclass, fields


@dataclass(frozen=True)
class ForcedType:
    """Converts primitive typed attributes automatically"""

    @staticmethod
    def is_complex(obj):
        return hasattr(obj, "__dict__")

    def __post_init__(self):
        for field in fields(self):
            value = getattr(self, field.name)
            if ForcedType.is_complex(value):
                continue
            object.__setattr__(self, field.name, field.type(value))


@dataclass(frozen=True)
class MinistatMeasurement(ForcedType):
    samples: int
    min: int
    max: int
    median: int
    avg: float
    stddev: float

    @property
    def variance(self) -> float:
        return self.stddev * self.stddev


@dataclass(frozen=True)
class MinistatResults(ForcedType):
    absolute_change: float
    absolute_error: float
    relative_change: float
    relative_error: float
    before: MinistatMeasurement
    after: MinistatMeasurement

    @property
    def relative_data(self) -> tuple[float, float]:
        return self.relative_change, self.relative_error

    @property
    def medians(self) -> tuple[int, int]:
        return self.before.median, self.after.median

    def is_significant(self, threshold=1) -> bool:
        return abs(self.relative_change) >= threshold

    def are_similar(self, other) -> bool:
        relative_difference = abs(self.relative_change - other.relative_change)

        return relative_difference < 5
