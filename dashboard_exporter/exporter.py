#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from pathlib import Path

import fire
from grafana_client import GrafanaApi
from grafana_client.util import setup_logging

from dashboard_exporter.dashboard import DashboardExporter

logger = logging.getLogger(__name__)


class DashboardCLI:
    def create_or_update(self, config_file: Path, message=None, overwrite=True):
        """
        Create (if does not exist) Update the dashboard on Grafana based on
        configuration files

        :param config_file: path to the config file.
        The config file should be in YAML format and contain the title and uid
        of the dashboard.
        :param message: message for the update.
        :param overwrite: boolean for overwrite, default is true.
        The overwrite parameter is a boolean, if true it will
        overwrite the existing dashboard, otherwise it will only serve to create
        a new one.

        Grafana API pointers can be optionally configured with environment
        variables `GRAFANA_URL` and `GRAFANA_TOKEN`.
        """
        # Setup logging.
        setup_logging(level=logging.DEBUG)

        grafana_client = GrafanaApi.from_env()
        dashboard = DashboardExporter(config_file, grafana_client)
        dashboard.create_or_update(message, overwrite)


if __name__ == "__main__":
    fire.Fire(DashboardCLI)
