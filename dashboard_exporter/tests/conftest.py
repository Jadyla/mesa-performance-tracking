import json
from pathlib import Path
from typing import Any, Dict
from unittest.mock import MagicMock

import pytest
import yaml
from grafana_client import GrafanaApi

from dashboard_exporter.dashboard import DashboardExporter


@pytest.fixture
def grafana_api() -> GrafanaApi:
    return GrafanaApi()


@pytest.fixture
def dashboard_config_path(tmp_path, json_model_path: Path) -> Path:
    assert json_model_path.exists()

    config = {
        "dashboards": {
            "Mesa Performance Driver": {
                "json_model": json_model_path.name,
                "overrides": {
                    "uid": "overridden",
                    "datasource": {"uid": "db_overridden"},
                },
            }
        }
    }
    # Create a temporary file to hold the yaml content
    temp: Path = tmp_path / "dashboard_config.yaml"
    temp.write_text(yaml.dump(config))
    return temp


@pytest.fixture
def json_model_path(tmp_path) -> Path:
    model = {
        "uid": "cbDUg3HVz",
        "title": "Mesa Performance Driver",
        "panels": [
            {
                "title": "CPU Usage",
                "type": "graph",
                "datasource": {"type": "abc"},
                "targets": [{"refId": "A", "target": "cpu_usage"}],
            }
        ],
    }
    # Create a temporary file to hold the JSON content
    temp: Path = tmp_path / "json_model.json"
    temp.write_text(json.dumps(model))
    return temp


@pytest.fixture
def dashboard_config(dashboard_config_path) -> Dict[str, Any]:
    return yaml.safe_load(dashboard_config_path.read_text())


@pytest.fixture
def json_model(json_model_path) -> Dict[str, Any]:
    return yaml.safe_load(json_model_path.read_text())


@pytest.fixture
def overrides(dashboard_config, json_model):
    title = json_model["title"]
    return dashboard_config["dashboards"][title].get("overrides")


@pytest.fixture
def grafana_api_mock():
    mock = MagicMock()
    mock.dashboard.update_dashboard.return_value = {}
    yield mock


@pytest.fixture
def grafana_dashboard(dashboard_config_path, grafana_api_mock):
    yield DashboardExporter(
        dashboard_config=dashboard_config_path,
        grafana_api=grafana_api_mock,
    )
