import logging
from unittest.mock import MagicMock

logger = logging.getLogger(__name__)


def test_dashboard(grafana_dashboard):
    assert grafana_dashboard


def test_dashboard_update(grafana_dashboard, dashboard_config: dict):
    grafana_dashboard.create_or_update(message=None, overwrite=True)
    update_api_call: MagicMock = (
        grafana_dashboard.grafana_api.dashboard.update_dashboard
    )
    assert update_api_call.call_count == len(dashboard_config["dashboards"])
    expected_payload = {
        "dashboard": {
            "uid": "overridden",
            "title": "Mesa Performance Driver",
            "panels": [
                {
                    "title": "CPU Usage",
                    "type": "graph",
                    "datasource": {"type": "abc", "uid": "db_overridden"},
                    "targets": [{"refId": "A", "target": "cpu_usage"}],
                }
            ],
        },
        "overwrite": True,
        "message": None,
    }
    update_api_call.assert_called_with(expected_payload)
