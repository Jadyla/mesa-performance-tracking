import json
import logging
from dataclasses import dataclass
from pathlib import Path
from typing import Any

import yaml
from grafana_client import GrafanaApi
from grafana_client.client import GrafanaBadInputError

logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class DashboardConfig:
    cwd: Path
    title: str
    json_model_filename: str
    overrides: dict

    @property
    def json_model(self) -> Path:
        return self.cwd.resolve() / self.json_model_filename


class DashboardExporter:
    """
    The Dashboard class is an abstraction for tweaking Grafana dashboards.
    It takes in a dashboard config file in the form of a YAML file that
    overrides a JSON model and a GrafanaApi instance.
    Provides methods for creating or updating the dashboard and applying
    overrides to the dashboard json model.
    """
    def __init__(self, dashboard_config: Path, grafana_api: GrafanaApi) -> None:
        self.dashboard_config = Path(dashboard_config)
        self.read_config()
        self.grafana_api: GrafanaApi = grafana_api

    def read_config(self) -> None:
        dashboards = yaml.safe_load(self.dashboard_config.read_text())
        self.configs: list[DashboardConfig] = []
        for dashboard_title, config in dashboards["dashboards"].items():
            json_model = config["json_model"]
            overrides = config["overrides"]
            cwd = self.dashboard_config.parent
            self.configs.append(
                DashboardConfig(cwd, dashboard_title, json_model, overrides)
            )

    @staticmethod
    def _override(data: dict, pieces: dict) -> None:
        if not isinstance(data, (dict, list)):
            return

        for key, value in data.items() if isinstance(data, dict) else enumerate(data):
            # Annotations field has its own datasource, for example. Let's keep it
            # intact for the moment.
            if key == "annotations":
                continue
            for piece, complement in pieces.items():
                if key == piece:
                    value |= complement
                    continue
                DashboardExporter._override(value, pieces)

    def _wrap_json_model(
        self,
        config: dict[str, Any],
        message: str = None,
        overwrite: bool = True,
    ) -> dict:
        payload = json.loads(config.json_model.read_text())
        if "id" in payload:
            # Removing id will force Grafana to create a new dashboard.
            # Uniqueness is guaranteed by uid
            del payload["id"]
        payload["title"] = config.title
        payload["uid"] = config.overrides.pop("uid")
        self._override(data=payload, pieces=config.overrides)

        return {
            "dashboard": payload,
            "overwrite": overwrite,
            "message": message,
        }

    def _update(self, config, message: str = None, overwrite: bool = True) -> None:
        dashboard_payload = self._wrap_json_model(config, message, overwrite)
        response = {}
        try:
            response = self.grafana_api.dashboard.update_dashboard(dashboard_payload)
        except GrafanaBadInputError as input_error:
            logger.error(
                f"""Could not update dashboard from {config.title} due to:
                {input_error}
                """
            )
            raise
        # Display the outcome in JSON format.
        logger.debug("Grafana response:")
        logger.debug(json.dumps(response, indent=2))

    def create_or_update(self, message: str = None, overwrite: bool = True) -> None:
        for config in self.configs:
            try:
                self._update(config, message, overwrite)
            except GrafanaBadInputError:
                logger.error("Proceeding with other files...")
