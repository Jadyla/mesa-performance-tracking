#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

"""
This module provides a function to extract and upload trace data from GitLab
pipelines to InfluxDB.

The run function connects to GitLab using the GitlabConnector class and
retrieves the pipelines that need to be processed.
For each pipeline, it uses the TraceDataPipelineUploader class to extract the
trace data, and upload it to InfluxDB using the InfluxDBDatasource class.
If the replay flag is set to True, the function will retrieve pipelines from a
specific branch, otherwise it will get the pipelines from merge requests.

"""

import logging
from dataclasses import dataclass, field
from datetime import datetime
from typing import Any, Iterable, Optional

import fire
from dateutil import parser
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import Project, ProjectCommit, ProjectPipeline
from influxdb_client import WritePrecision
from influxdb_client.rest import ApiException
from wrappers.gitlab import GitlabConnector
from wrappers.influxdb import InfluxdbDictWriter, InfluxdbSource

logging.basicConfig(level=logging.DEBUG)


def get_source_data(commit: ProjectCommit) -> tuple[ProjectCommit, datetime]:
    """
    Get the source commit and the estimated pipeline time for a replayed job.

    Args:
    commit (ProjectCommit): The commit object for which to get the source commit
    and estimated pipeline time.

    Returns:
    tuple[ProjectCommit, datetime]: A tuple containing the source commit and the
    estimated pipeline time.
    """
    gl: GitlabConnector = GitlabConnector("mesa/mesa")
    source_commit_guess: str = commit.message.split("\n")[2]
    try:
        source_commit: ProjectCommit = gl.project.commits.get(source_commit_guess)
    except GitlabGetError:
        logging.error(
            f"Could not find source commit {source_commit} for replayed jobs "
            f"in remote {gl.project_path}"
        )
        raise
    source_pipeline: ProjectPipeline = gl.project.pipelines.list(
        sha=source_commit.id, source="merge_request_event"
    )[0]
    estimated_pipeline_time: datetime = parser.parse(source_pipeline.finished_at)
    return source_commit, estimated_pipeline_time


def average_trace_jobs_time(source_pipeline) -> datetime:
    source_trace_job_timestamps: list[float] = [
        datetime.fromisoformat(j.finished_at[:-1]).timestamp()
        for j in source_pipeline.jobs.list()
        if "-traces" in j.name
    ]
    average_trace_job_timestamps: float = sum(source_trace_job_timestamps) / len(
        source_trace_job_timestamps
    )
    return datetime.fromtimestamp(average_trace_job_timestamps)


class PerformanceTraceDictWriter:
    """Class to handle the creation of InfluxDB data source

    Attributes:
        tags (tuple): Tuple of strings representing the InfluxDB tags for the data source.
        fields (tuple): Tuple of strings representing the InfluxDB fields for the data source.
    """

    tags = ("job_name", "trace_name", "project_path")
    fields = ("pipeline_id", "commit_sha", "commit_title", "job_id", "frame_time")

    @staticmethod
    def extract_metadata(
        job, pipeline, project_path, commit, **kwargs
    ) -> dict[str, Any]:
        return {
            "job_name": job.name,
            "project_path": project_path,
            "pipeline_id": pipeline.id,
            "commit_sha": commit.short_id,
            "commit_title": commit.title,
            "job_id": job.id,
            **kwargs,
        }


class TraceDataPipelineUploader:
    """Uploader class for trace data pipeline.

    This class is responsible for uploading trace data pipeline to InfluxDB. It uses
    the Gitlab connector to retrieve results from the Gitlab API and InfluxDB
    connector to write the results to InfluxDB.
    """

    def __init__(
        self,
        source: InfluxdbSource,
        gl: GitlabConnector,
        pipeline: ProjectPipeline,
        replay=False,
    ) -> None:
        self.source: InfluxdbSource = source
        self.gl: GitlabConnector = gl
        self.pipeline: ProjectPipeline = pipeline
        self.project: Project = self.gl.project
        self.project_path: str = self.gl.project_path
        self.commit: ProjectCommit = self.project.commits.get(self.pipeline.sha)
        self.datasource: InfluxdbDictWriter = InfluxdbDictWriter(
            PerformanceTraceDictWriter.tags,
            PerformanceTraceDictWriter.fields,
            self.source,
        )
        self.replay: bool = replay
        self.source_commit: Optional[ProjectCommit] = None
        self.found_data: bool = False

        if self.replay:
            self.source_commit, self.pipeline_time = get_source_data(self.commit)
        else:
            self.pipeline_time: datetime = parser.parse(self.pipeline.finished_at)  # type: ignore

    def get_trace_jobs(self):
        jobs = []

        logging.debug(f"Retrieving jobs for pipeline {self.pipeline.id}")
        for job in self.pipeline.jobs.list(iterator=True, per_page=150):
            if job.status == "created":
                continue
            if "-traces-performance" in job.name:
                jobs.append(job)

        return jobs

    def get_trace_frame_times(self, results, trace):
        """- `return` The frame times of a specified trace within the specified results"""

        if results["tests"][trace]["images"] is None:
            return []

        if "frame_times" not in results["tests"][trace]["images"][0]:
            return []

        if frame_times := results["tests"][trace]["images"][0]["frame_times"]:
            # Ignore last frame time, tends to be quite off
            frame_times.pop()

            # Remove negative values related to a bug with some drivers profiling unit
            # scale measurements
            frame_times = [t for t in frame_times if t > 0]

        return frame_times

    def extract_trace_frame_timings(self, trace, metadata, results, job) -> None:
        if not trace.endswith(".trace"):
            return

        frame_times = self.get_trace_frame_times(results, trace)
        if not frame_times:
            return

        self.found_data = True
        # Name of the trace that has been replayed
        metadata["trace_name"] = "/".join(trace.split("@")[2:])

        logging.debug(
            "Adding %d frames for trace %s from job %s"
            % (len(frame_times), trace, job.name)
        )
        for frame_no, frame_time in enumerate(frame_times):
            # Time for a frame to hit presentation in nanoseconds
            metadata["frame_time"] = frame_time
            self.datasource.insert_data(
                "frame_time",
                metadata,
                InfluxdbDictWriter.increment_timing(self.pipeline_time, frame_no),
                write_precision=WritePrecision.NS,
            )

    def upload_job_data(self, job) -> None:
        results: Optional[dict[str, Any]] = self.gl.get_results(job)

        # Skip jobs with no results
        if results is None:
            return

        metadata: dict[str, Any] = PerformanceTraceDictWriter.extract_metadata(
            job=job,
            pipeline=self.pipeline,
            project_path=self.project_path,
            commit=self.source_commit if self.replay else self.commit,
            replay=self.replay,
        )

        # Get frame times for apitrace traces
        for trace in results["tests"]:
            self.extract_trace_frame_timings(trace, metadata, results, job)

    def upload_data(self) -> None:
        logging.info(f"Pipeline: {self.pipeline.web_url}")

        for job in self.get_trace_jobs():
            self.upload_job_data(job)


@dataclass
class TraceDataPipelineManager:
    """
    A class responsible for retrieving pipelines from either the main branch or
    a specific branch, and uploading the corresponding trace data to the
    InfluxDB datasource.

    This script is covered by python-fire, which enables the user to run this script
    in various use cases.

    Examples:
    ```
    # Get the pipelines from the main branch and upload trace data to InfluxDB
    python driver-trace-stats.py run

    # Retrieve the pipelines from the main branch and output them as a list
    python driver-trace-stats.py get_pipelines --as_list

    # Get the pipelines from a specific branch and upload trace data to InfluxDB
    python driver-trace-stats.py pipeline_args --ref="specific-branch-name" run

    # Get the pipelines from a specific branch and output them as a list
    python driver-trace-stats.py get_pipelines --ref="specific-branch-name" --as_list

    # Get the pipelines from the main branch that were updated after a certain date
    # and upload trace data to InfluxDB
    python driver-trace-stats.py --last-write="2022-12-01" run

    # Get the pipelines from the main branch that were updated before a certain date
    # after the default date, and output them as a list
    python driver-trace-stats.py get_pipelines --as_list --updated-before="2023-01-01"

    # Run for pipelines from a specific branch that were updated between two dates
    # and upload trace data to InfluxDB
    python driver-trace-stats.py --ref="specific-branch-name" \
        --last-write="2022-12-01" pipeline_args --updated-before="2023-01-01" \
        run
    ```
    """

    gl: GitlabConnector = GitlabConnector()
    db_source: InfluxdbSource = InfluxdbSource("mesa-perf-v2")
    replay: bool = False
    last_write: str = ""
    _pipeline_args: dict[str, Any] = field(default_factory=dict, init=False)

    def __post_init__(self) -> None:
        """
        Initializes the TraceDataPipelineManager instance. If `last_write` is
        not provided, sets it to the most recent value of "frame_time" in the
        InfluxDB datasource.
        """
        if self.last_write:
            return

        self.last_write = self.db_source.get_last_write(
            measurement="frame_time", field="frame_time"
        ).isoformat()

    def pipeline_args(self, **kwargs):
        """
        Sets additional pipeline arguments to be passed to `gl.main_pipelines_from_mrs` or
        `gl.pipelines_from_branch`. Mainly to be used to interact with
        python-fire and run commands.

        Example:
        ```
        # List the pipelines with default arguments, but override the source one
        # with "push" type
        python driver-trace-stats.py pipeline_args --source="push" get_pipelines

        # Run the script with custom arguments
        python driver-trace-stats.py --last-write="2022-12-01" \
            pipeline_args --updated-before="2023-01-01" \
                run
        ```

        Args:
            **kwargs: Additional keyword arguments to be passed.

        Returns:
            A `TraceDataPipelineRunner` instance with the updated `_pipeline_args`.
        """
        self._pipeline_args = kwargs
        return self

    def get_pipelines(self, as_list=False, **kwargs) -> Iterable[ProjectPipeline]:
        """
        Retrieves pipelines either from the main branch or a specific branch.

        Args:
            as_list: Whether to return the pipelines as a list.
            **kwargs: Additional keyword arguments to be passed.

        Returns:
            An iterable containing the retrieved pipelines.
        """
        # When replaying, we need to get the pipelines from a specific branch, not
        # from merge requests.
        pipelines: Iterable[ProjectPipeline] = (
            self.gl.pipelines_from_branch(finished_after=self.last_write, **kwargs)
            if self.replay
            else self.gl.main_pipelines_from_mrs(
                merged_after=datetime.fromisoformat(self.last_write),
                **kwargs,
            )
        )

        return list(pipelines) if as_list else pipelines

    def run(self) -> None:
        """
        Retrieves pipelines and uploads the corresponding trace data to the
        InfluxDB datasource.
        """
        for pipeline in self.get_pipelines(**self._pipeline_args):
            try:
                with self.db_source.writer_instance():
                    trace_miner: TraceDataPipelineUploader = TraceDataPipelineUploader(
                        self.db_source, self.gl, pipeline, replay=self.replay
                    )
                    trace_miner.upload_data()
                    if trace_miner.found_data:
                        logging.debug(
                            f"Added data for pipeline {trace_miner.pipeline.id} at "
                            f"{trace_miner.pipeline_time}"
                        )
            except GitlabGetError:
                # Normally happens when replaying
                continue
            except ApiException as influx_api_error:
                logging.error(
                    f"Failed to add data for pipeline {trace_miner.pipeline.id}: {influx_api_error}"
                )


if __name__ == "__main__":
    fire.Fire(TraceDataPipelineManager)
