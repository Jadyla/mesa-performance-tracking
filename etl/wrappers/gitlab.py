#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import bz2
import json
import logging
import time
from dataclasses import dataclass
from datetime import datetime
from functools import singledispatchmethod
from os import environ
from typing import Any, ClassVar, Generator, Optional

import requests
from gitlab.base import RESTObjectList
from gitlab.client import Gitlab
from gitlab.v4.objects import (
    Project,
    ProjectMergeRequest,
    ProjectPipeline,
    ProjectPipelineJob,
)
from gitlab.exceptions import GitlabGetError


@dataclass
class GitlabConnector:
    _default_pipeline_args: ClassVar[dict[str, Any]] = {
        "iterator": True,
        # The default value of this parameter is 20, let's increase it to reduce
        # the number of API calls, balancing between bandwidth and TTL.
        "per_page": 160,
    }
    _project_path: str = environ.get("GITLAB_PROJECT_PATH", "mesa/mesa")
    _url: str = environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
    _gitlab_token: str = environ.get("GITLAB_TOKEN", "<token unspecified>")
    _username: str = environ.get("GITLAB_USER", "marge-bot")
    pipeline_wait_time_sec: int = 2

    def __post_init__(self) -> None:
        self.__gl: Gitlab = Gitlab(
            url=self._url,
            private_token=self._gitlab_token,
            retry_transient_errors=True,
        )
        self._project: Project = self.__gl.projects.get(self._project_path)

    @property
    def project(self) -> Project:
        return self._project

    @property
    def project_path(self) -> str:
        return self._project_path

    @singledispatchmethod
    def gitlab_timeformat(self, timestamp: str) -> str:
        return timestamp

    @gitlab_timeformat.register
    def _(self, timestamp: datetime) -> str:
        return timestamp.isoformat()

    def pipelines_from_branch(
        self,
        remote_branch: str = "main",
        source: str = "merge_request_event",
        **kwargs,
    ) -> list[ProjectPipeline]:
        """
        Retrieve pipelines from a specific branch of the GitLab project.

        Args:
            remote_branch (str, optional): The branch to retrieve pipelines
            from. Defaults to "main".
            **kwargs: Additional arguments to pass to the GitLab API.

        Returns:
            list[ProjectPipelineJob]: A list of project pipeline jobs.
        """
        list_args = {
            **self._default_pipeline_args,
            "ref": remote_branch,
        } | kwargs
        return list(self._project.pipelines.list(**list_args))  # type: ignore

    def mrs_in_main_merged_after(
        self, merged_after: datetime, **kwargs
    ) -> Generator[ProjectMergeRequest, None, None]:
        """
        Retrieve merge requests targeting the "main" branch.

        Args:
            merged_after (datetime): Only return MRs merged after the specified
            date.
            **kwargs: Additional arguments to pass to the GitLab API.

        Returns:
            Generator[ProjectMergeRequest, None, None]: A generator of project MRs.
        """
        list_args = {
            **self._default_pipeline_args,
            "updated_after": merged_after.isoformat(),
            "target_branch": "main",
            "state": "merged",
            **kwargs,
        }
        for mr in self._project.mergerequests.list(**list_args):
            # MRs can be updated after a merge, so we need to guarantee that
            # we are only looking at the merged date, unfortunately, Gitlab does
            # not support that.
            if datetime.fromisoformat(
                mr.merged_at.rstrip("Z")
            ) <= merged_after.replace(tzinfo=None):
                continue
            # items from list only comes in partial form, using get() to get all info.
            yield self._project.mergerequests.get(mr.iid)

    def main_pipelines_from_mrs(
        self, merged_after: datetime, **kwargs
    ) -> Generator[ProjectPipeline, None, None]:
        """
        Retrieve pipelines from merge requests targeting the "main" branch which
        were merged after a specific date.

        Args:
            merged_after (datetime): Only return end pipelines merged after the
            specified date.
            **kwargs: Additional arguments to pass to the GitLab API.

        Returns:
            Generator[ProjectPipeline, None, None]: A generator of project pipelines.
        """
        list_args = {
            **self._default_pipeline_args,
            "merged_after": merged_after,
            "target_branch": "main",
            "state": "merged",
            **kwargs,
        }
        for mr in self.mrs_in_main_merged_after(**list_args):
            yield self._project.pipelines.get(mr.pipeline["id"])

    def pipeline_list(
        self, updated_after: datetime, **kwargs
    ) -> Generator[ProjectPipelineJob, None, None]:
        list_args = {
            **self._default_pipeline_args,
            "sort": "asc",
            "updated_after": updated_after.isoformat(),
            "username": self._username,
        } | kwargs
        yield from self._project.pipelines.list(**list_args)  # type: ignore

    def wait_for_pipeline(self, sha):
        while True:
            if pipelines := self._project.pipelines.list(sha=sha):
                try:
                    return pipelines[0]
                except AttributeError:
                    pipelines: RESTObjectList
                    return pipelines.next()
            time.sleep(self.pipeline_wait_time_sec)

    def get_pipeline(self, pipeline_id: int) -> ProjectPipeline:
        return self._project.pipelines.get(pipeline_id)

    @staticmethod
    def get_results(job: ProjectPipelineJob) -> Optional[dict[str, Any]]:
        """- `return` A Results object with the results artifacts of the specified job"""
        if job.pipeline["status"] == "running":
            logging.warning(f'Pipeline {job.pipeline["id"]} still running')
            return None

        url = f"{job.web_url}/artifacts/raw/results/results.json.bz2"
        print(f"Requesting results from: {url}")
        r = requests.get(url)

        if r.status_code != 200:
            print(f"Failed to download ({r.status_code}):\n\t{url}")
            return None

        return json.loads(bz2.decompress(r.content))

    @staticmethod
    def get_artifact(job: ProjectPipelineJob, path: str):
        try:
            return job.artifact(path).decode("UTF-8")
        except GitlabGetError as e:
            # File doesn't exist, ignore
            if e.response_code != 404:
                raise
            logging.debug(
                f"Artifact {path} doesn't exist for job {job.web_url}"
            )
        return None
