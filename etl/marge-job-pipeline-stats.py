#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import logging
from typing import Tuple

from gitlab.v4.objects.pipelines import ProjectPipeline, ProjectPipelineJob
from wrappers.gitlab import GitlabConnector
from wrappers.influxdb import InfluxdbDictWriter, InfluxdbSource


def from_object_to_dict(object, attributes: Tuple, prefix: str = ""):
    return {prefix + attr: getattr(object, attr) for attr in attributes}


def update_job_data(source: InfluxdbSource, pipeline):
    tags = ("status", "stage", "name")
    fields = ("pipeline_id", "duration", "id", "created_at")
    datasource = InfluxdbDictWriter(tags, fields, source)

    job: ProjectPipelineJob
    for job in pipeline.jobs.list(iterator=True, per_page=150):
        job_attrs = tuple(tags + fields)
        job_dict = from_object_to_dict(job, job_attrs)
        if job.duration:
            job_dict["duration"] = float(job.duration)
        datasource.insert_data("job", job_dict, job.created_at)


def upload_pipeline_data(source: InfluxdbSource, pipeline):
    tags = ("status",)
    fields = ("id", "created_at", "sha")
    datasource = InfluxdbDictWriter(tags, fields, source)
    pipeline_attrs = tuple(tags + fields)
    pipeline_dict = from_object_to_dict(pipeline, pipeline_attrs)
    datasource.insert_data("pipeline", pipeline_dict, pipeline.updated_at)
    return pipeline_dict


def upload_data(source: InfluxdbSource, gl: GitlabConnector, pipeline):
    print("Pipeline: %s" % pipeline.web_url)

    upload_pipeline_data(source, pipeline)

    update_job_data(source, pipeline)


def main():
    logging.basicConfig(level=logging.DEBUG)
    source = InfluxdbSource("marge-stats")
    gl = GitlabConnector()

    last_write = source.get_last_write(measurement="pipeline", field="id")
    # Uncomment below to force data extraction since the specified start range.
    # last_write = source.get_start_date()

    pipeline: ProjectPipeline
    for pipeline in gl.pipeline_list(
        last_write,
        source="merge_request_event",
    ):
        with source.writer_instance():
            upload_data(source, gl, pipeline)


if __name__ == "__main__":
    main()
