#!/usr/bin/python3

# Copyright © 2021,2023 Collabora Ltd.
# SPDX-License-Identifier: MIT

"""
This script automates the process of triggering performance jobs for GitLab
pipelines associated with merge requests that were merged into the main branch.
It identifies untriggered performance jobs, fetching them based on the merge
requests' target branch and merge date, and triggers these jobs in a
chronological order.

Main functionality:

- Filters merge requests targeting the main branch and merged within the last day.
- Sorts the pipelines based on their merge date in descending order.
- Retrieves performance jobs from the given pipeline by filtering jobs with
names that end with "-traces-performance".
- Checks whether any performance jobs have already been triggered by examining
their status.
- Processes the performance jobs for a given pipeline and checks if any of them
are already triggered. If any triggered jobs are found, raises the
PerformanceJobsAlreadyTriggered exception.
- Triggers untriggered performance jobs in a chronological order.
"""

import logging
import os
from datetime import datetime, timedelta, timezone
from operator import attrgetter
from typing import Any, Iterable

from gitlab import GitlabJobPlayError
from gitlab.v4.objects import ProjectJob

from etl.wrappers.gitlab import GitlabConnector, ProjectPipeline, ProjectPipelineJob


class PerformanceJobsAlreadyTriggered(Exception):
    pass


def create_terminal_hyperlink(url: str, text: str) -> str:
    """
    Create a hyperlink in the terminal.

    :param url: The target URL of the hyperlink
    :param text: The text to display as the hyperlink

    :returns: The hyperlink as a string
    """

    # If it is under Gitlab CI, don't create the hyperlink
    if "CI" in os.environ:
        return f"[{text}]({url})"

    return f"\x1b]8;;{url}\x1b\\{text}\x1b]8;;\x1b\\"


def generate_id_link(gl_obj: Any) -> str:
    return create_terminal_hyperlink(gl_obj.web_url, gl_obj.web_url)


def extract_performance_jobs(pipeline: ProjectPipeline) -> list[ProjectPipelineJob]:
    logging.info(f"Processing pipeline {pipeline.id}")
    return [
        job for job in pipeline.jobs.list(iterator=True) if job.name.endswith("-traces-performance")
    ]


def is_job_triggered(job: ProjectPipelineJob) -> bool:
    """Check if any job is already triggered."""
    non_triggered_status = ("manual", "skipped", "created")
    return job.status not in non_triggered_status


def are_jobs_triggered(jobs: Iterable[ProjectPipelineJob]) -> bool:
    """Check if any jobs in the given list are already triggered.

    :param jobs: Iterable of performance jobs to check.

    :returns: True if any performance jobs have been triggered, False otherwise.
    """
    return any((is_job_triggered(job) for job in jobs))


def check_performance_jobs(
    pipeline: ProjectPipeline, performance_jobs: Iterable[ProjectPipelineJob]
) -> None:
    logging.info(f"Pipeline {generate_id_link(pipeline)} had these performance jobs")
    for job in performance_jobs:
        logging.info(f"{job.name}: {generate_id_link(job)}")
        logging.info("")

    # Stop once we reach the first pipeline with performance jobs that have
    # been triggered already
    if are_jobs_triggered(performance_jobs):
        logging.warning(
            f"Pipeline {generate_id_link(pipeline)} had all performance jobs triggered already"
        )
        raise PerformanceJobsAlreadyTriggered


def find_triggerable_performance_jobs(
    pipelines: Iterable[ProjectPipeline],
) -> list[ProjectPipelineJob]:
    jobs_to_trigger: list[ProjectPipelineJob] = []
    for pipeline in pipelines:
        if performance_jobs := extract_performance_jobs(pipeline):
            try:
                check_performance_jobs(pipeline, performance_jobs)
            except PerformanceJobsAlreadyTriggered:
                logging.info(
                    "Early exiting as past pipelines should have all performance jobs triggered"
                )
                break
            jobs_to_trigger.extend(performance_jobs)
        logging.info(f"Pipeline {pipeline.id} has no performance jobs")

    # The order of the pipelines are from newest to oldest. Reverse the order of
    # the jobs to be chronological.
    return reversed(jobs_to_trigger)


def trigger_jobs(gl: GitlabConnector, jobs: Iterable[ProjectPipelineJob]) -> None:
    for job in jobs:
        logging.info(f"Triggering job {job.name}: {generate_id_link(job)}")
        pjob: ProjectJob = gl.project.jobs.get(job.id, lazy=True)
        try:
            pjob.play()
        except GitlabJobPlayError as e:
            logging.error(f"Failed to trigger job {job.name} (status = {job.status}): {e}")
            continue


def main() -> None:
    # Logging with timestamps
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    gl = GitlabConnector()

    # Limit to 1 day
    since: datetime = datetime.now(timezone.utc) - timedelta(days=1)

    # Performance jobs are post-merged ones.
    # Look only for pipelines which caused an MR to be merged.
    merged_mrs = gl.mrs_in_main_merged_after(since)

    # This script needs to find out which was the last pipeline that triggered
    # their performance jobs. To know when to stop when rerunning the script, it
    # sorts pipelines by merge date and exits as soon as it finds the first
    # pipeline that has already run its performance jobs.
    logging.info("Finding the right pipelines. This may take a while (< 1min)...")
    latest_end_pipelines = (
        gl.project.pipelines.get(mr.pipeline["id"])
        for mr in sorted(merged_mrs, key=attrgetter("merged_at"), reverse=True)
    )

    triggerable_jobs: list[ProjectPipelineJob] = find_triggerable_performance_jobs(
        latest_end_pipelines
    )
    trigger_jobs(gl, triggerable_jobs)


if __name__ == "__main__":
    main()
