version: '3.8'

# create YAML anchor for default service network
x-default:
  &base-config
  networks:
    - monitoring-network
  security_opt:
    - apparmor:unconfined

services:
  influxdb:
    <<: *base-config
    image: docker.io/influxdb:2.2-alpine
    container_name: influxdb
    hostname: influxdb
    volumes:
      - ./influxdb-scripts:/docker-entrypoint-initdb.d:Z
      - time-series-db:/var/lib/influxdb2:Z
    environment:
      DOCKER_INFLUXDB_INIT_MODE: setup
      DOCKER_INFLUXDB_INIT_USERNAME: admin
      DOCKER_INFLUXDB_INIT_PASSWORD: adminadmin
      DOCKER_INFLUXDB_INIT_ORG: freedesktop
      DOCKER_INFLUXDB_INIT_BUCKET: mesa-perf-v2
      DOCKER_INFLUXDB_INIT_RETENTION: 12w
      DOCKER_INFLUXDB_INIT_ADMIN_TOKEN: influx-testing-token
    expose:
      - 8086
    ports:
      - 8086:8086

  grafana:
    <<: *base-config
    image: docker.io/grafana/grafana:9.2.4
    container_name: grafana
    hostname: grafana
    volumes:
      - ./grafana-provisioning:/etc/grafana/provisioning:Z
      # map external volume to grafana
      - dashboards-db:/var/lib/grafana:Z
    environment:
      GF_PATHS_PROVISIONING: "/etc/grafana/provisioning"
    expose:
      - 3000
    ports:
      - 3000:3000

  gitlab-runner:
    <<: *base-config
    image: docker.io/gitlab/gitlab-runner:alpine
    container_name: gitlab-runner
    hostname: gitlab-runner
    privileged: true
    volumes:
      - ${DOCKER_SOCK_VOLUME}
    environment:
      CI_SERVER_URL: "https://gitlab.freedesktop.org/"
      REGISTRATION_TOKEN: "${RUNNER_REGISTRATION_TOKEN}"
      REGISTER_NON_INTERACTIVE: "true"
      RUNNER_NAME: mesa-performance-tracking
      RUNNER_EXECUTOR: "docker"
      RUNNER_CONCURRENT_BUILDS: "4"
      RUNNER_REQUEST_CONCURRENCY: "4"
      DOCKER_IMAGE: "alpine"
      DOCKER_NETWORK_MODE: "monitoring-network"
      GRAFANA_HOST: grafana
      INFLUX_URL: http://influxdb:8086
      DOCKER_PRIVILEGED: "true"
      RUNNER_TAG_LIST: "placeholder-job"
    entrypoint:
      - "sh"
      - "-cx"
    command:
      - |
        echo "concurrent = 4" > /etc/gitlab-runner/config.toml &&
        /entrypoint unregister --all-runners &&
        /entrypoint register --run-untagged &&
        /usr/bin/dumb-init /entrypoint run \
          --user=gitlab-runner \
          --working-directory=/home/gitlab-runner

volumes:
  # To safeguard the local data from deletion if the container is removed, it's
  # advisable to utilize an external volume for storing monitoring data. To
  # create a volume in a docker command, refer to the README for further
  # details.
  time-series-db:
    external: true
    name: influxdb-data
  dashboards-db:
    external: true
    name: dashboards-data

networks:
  # To prevent problems with docker-compose, it's essential to give the bridge
  # network an explicit name. This is especially important if you're using
  # git-worktree, as different folders for the same repository can cause naming
  # issues in the default network behavior. These issues can be challenging to
  # troubleshoot, so it's best to be proactive and explicitly name the network.
  monitoring-network:
    name: monitoring-network
